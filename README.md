# todovue

## ДЛЯ ЗАПУСКА ПРИЛОЖЕНИЯ:
```
docker-compose up
```

## ПРИЛОЖЕНИЕ ДОСТУПНО НА 8080 ПОРТУ:
```
http://127.0.0.1:8080
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
