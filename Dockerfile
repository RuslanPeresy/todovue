FROM node:alpine
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build
RUN npm install -g http-server json-server
RUN chmod +x entrypoint.sh
CMD ["./entrypoint.sh"]