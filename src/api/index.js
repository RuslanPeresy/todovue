import axios from 'axios';

const baseURL = 'http://localhost:3000';

export {
    baseURL
};

const $http = axios.create({
    baseURL,
});

export default $http;