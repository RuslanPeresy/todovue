export default {
    name: 'Modal',
    props: ["show", "noteid", "pageType"],
    methods: {
        closeModal() {
            this.$emit('close')
        },
        undoAction() {
            this.$root.$emit('undoAction');
            this.$emit('close');
        },
        deleteNote(id) {
            this.$root.$emit('deleteNote', id);
            this.$emit('close');
        }
    }
};