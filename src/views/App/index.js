import Layout from '@/views/Layout/index.vue';

export default {
    name: 'App',
    components: {
        Layout
    }
};