import { mapGetters } from 'vuex';
import Modal from '@/components/Modal/index.vue';

export default {
    name: 'Note',
    components: {
        Modal
    },
    data() {
        return {
            editTodo: 0,
            redoState: {},
            showModal: false,
            pageType: 'note',
            lastModifiedId: 0
        };
    },
    methods: {
        async deleteTodo(noteid, todoid) {
            await this.$store.dispatch('deleteTodo', todoid);
            await this.$store.dispatch('fetchTodos', noteid);
        },
        async saveTodo() {
            const todo = this.noteTodos.filter(obj => {
                return obj.id === this.editTodo })[0];

            await this.$store.dispatch('saveTodo', todo);
            
            this.lastModifiedId = todo.id;
            this.editTodo = 0;
        },
        async createTodo() {
            if(this.newTodo.name != ''){
                await this.$store.dispatch('createTodo', {
                    noteId: this.currentNote.id,
                    name: this.newTodo.name,
                    description: this.newTodo.description,
                    done: false
                })
            }
            console.log(this.currentNote.id);
        },
        async updateTodoStatus(todo) {
            await this.$store.dispatch('saveTodo', todo);
        },
        async undo() {
            if(this.lastModifiedId != 0) {
                const state = await this.$store.dispatch('undoEdition', this.lastModifiedId);
                this.redoState = state;
                await this.$store.dispatch('fetchTodos', this.$route.params.id);
            }
        },
        async redo() {
            if(Object.keys(this.redoState).length != 0) {
                await this.$store.dispatch('redoEdition', {
                    state: this.redoState,
                    id: this.lastModifiedId
                });
                await this.$store.dispatch('fetchTodos', this.$route.params.id);
            }
            this.redoState = {};
        }
    },
    computed: mapGetters(["currentNote", "noteTodos", "newTodo"]),
    async mounted() {
        this.$store.dispatch('fetchNote', {
            id: this.$route.params.id,
        });
        this.$store.dispatch('fetchTodos', this.$route.params.id);

        this.$root.$on('undoAction', this.undo);
        this.lastModifiedId = 0;
    },
};