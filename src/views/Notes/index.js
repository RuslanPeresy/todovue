import Modal from '@/components/Modal/index.vue';
import { mapGetters } from 'vuex';

export default {
  name: 'Notes',
  components: {
    Modal,
  },
  data() {
    return {
      showModal: false,
      noteId: 0,
      pageType: 'main',
    };
  },
  computed: mapGetters(["allNotes", "allTodos"]),
  methods: {
    async onDelete(id) {
      await this.$store.dispatch('deleteNote', id );
    },
    async onCreate(title) {
      if(title != ''){
        await this.$store.dispatch('createNote', {
          title
        });
        await this.$store.dispatch('fetchNotes');
      }
    },
    getNoteTodos(noteid) {
      const todos = this.allTodos.filter(obj => {
        return obj.noteId === noteid }).slice(0, 5);
      return todos;
    }
  },
  async mounted() {
    this.$store.dispatch('fetchNotes');
    this.$store.dispatch('fetchAllTodos');

    this.$root.$on('deleteNote', this.onDelete);
  },
};