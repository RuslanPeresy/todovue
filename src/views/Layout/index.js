export default {
    name: 'Layout',
    props: {
      small: {
        type: Boolean,
        default: false,
      },
    },
  };
  