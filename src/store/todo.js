import $http from '@/api';
import undoHistory from './undoHist';

export default {
    state: {
        allTodos: [],
        todos: [],
        todo: {}
    },
    mutations: {
        setAllTodos(state, todos) {
            state.allTodos = todos
        },
        updateTodos(state, todos) {
            state.todos = todos
        },
        createTodo(state, todo) {
            state.todos.push(todo);
            state.todo.name = '';
            state.todo.description = '';
        }
    },
    actions: {
        async fetchAllTodos(ctx) {
            await $http
                .get('/todos')
                .then(response => (ctx.commit
                    ('setAllTodos', response.data)));
        },
        async fetchTodos(ctx, noteid) {
            await $http
                .get(`/notes/${noteid}/todos`)
                .then(response => (ctx.commit
                    ('updateTodos', response.data)));
        },
        async deleteTodo(ctx, id) {
            await $http
                .delete(`/todos/${id}`)
        },
        async saveTodo(ctx, todo) {
            await $http
                .put(`/todos/${todo.id}`, {
                    noteId: todo.noteId,
                    name: todo.name,
                    description: todo.description,
                    done: todo.done
                });
        },
        async createTodo(ctx, todo) {
            await $http
                .post('/todos', todo)
                .then(ctx.commit('createTodo', todo));
        },
        async undoEdition(ctx, id) {
            undoHistory.undo();

            const oldState = ctx.state; 

            const history = undoHistory.history;
            
            const todos = history[history.length - 1].todo.todos;

            const todo = todos.filter(obj => {
                return obj.id === id })[0];
            await $http
                .put(`/todos/${id}`, {
                    noteId: todo.noteId,
                    name: todo.name,
                    description: todo.description,
                    done: todo.done
                });
            return oldState;
        },
        async redoEdition(ctx, { state, id }) {
            const todos = state.todos;

            const todo = todos.filter(obj => {
                return obj.id === id })[0];
            await $http
                .put(`/todos/${id}`, {
                    noteId: todo.noteId,
                    name: todo.name,
                    description: todo.description,
                    done: todo.done
                });
        }
    },
    getters: {
        allTodos(state) {
            return state.allTodos
        },
        noteTodos(state) {
            return state.todos
        },
        newTodo(state) {
            return state.todo
        }
    }
};