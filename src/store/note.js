import $http from '@/api';

export default {
    state: {
        notes: [],
        note: {},
    },
    mutations: {
        updateNotes(state, notes) {
            state.notes = notes
            state.note.title = '';
        },
        setNote(state, note) {
            state.note = note
        },
        deleteNote(state, id) {
            state.notes.splice(id-1, 1);
        },
        createNote(state) {
            state.note.title = '';
        }
    },
    actions: {
        async fetchNotes(ctx) {
            await $http
                .get('/notes')
                .then(response => (ctx.commit
                    ('updateNotes', response.data)));
        },
        async fetchNote(ctx, { id }) {
            await $http
                .get(`/notes/${id}`)
                .then(response => (ctx.commit
                    ('setNote', response.data)));
        },
        async deleteNote(ctx, id) {
            await $http
                .delete(`/notes/${id}`)
                .then(ctx.commit('deleteNote', id));
        },
        async createNote(ctx, { title }) {
            const note = { title: title }
            await $http
                .post('/notes', note)
                .then(ctx.commit('createNote'));
        }
    },
    getters: {
        allNotes(state) {
            return state
        },
        currentNote(state) {
            return state.note
        }
    }
};