const cloneDeep = require('lodash.clonedeep');

class UndoHistory {
    store;
    history = [];
    currentIndex = -1;

    init(store) {
        this.store = store;
    }

    addState(state) {
        if (this.currentIndex + 1 < this.history.length) {
            this.history.splice(this.currentIndex + 1);
        }
        this.history.push(state);
        this.currentIndex++;
    }

    undo() {
        const prevState = this.history[this.currentIndex - 1];
        this.store.replaceState(cloneDeep(prevState));
        this.currentIndex--;
    }
}

const undoHistory = new UndoHistory();

export default undoHistory;