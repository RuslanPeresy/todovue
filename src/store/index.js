import Vue from "vue";
import Vuex from "vuex";
import note from './note';
import todo from './todo';
import undoHistory from './undoHist';

Vue.use(Vuex);

const cloneDeep = require('lodash.clonedeep');

const undoPlugin = (store) => {
  undoHistory.init(store);
  let firstState = cloneDeep(store.state);
  undoHistory.addState(firstState);

  store.subscribe((mutation, state) => {
    undoHistory.addState(cloneDeep(state));
  });
}

export default new Vuex.Store({
  modules: {
    note,
    todo
  },
  plugins: [undoPlugin]
})
