import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import { mainNav } from './mainNav';
import { noteNav } from './noteNav';

export const routes = [
  ...mainNav,
  noteNav,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
