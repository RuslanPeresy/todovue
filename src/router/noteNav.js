const noteNav = {
    path: '/notes/:id',
    name: 'note',
    component: () => import('../views/Note/index.vue'),
    meta: {
        show: true,
        type: 'note',
    },
};

export { noteNav };