const mainNav = [
    {
        path: '/',
        name: 'home',
        component: () => import('../views/Home/index.vue'),
        redirect: { name: 'notes' },
        meta: {
            show: true,
            type: 'home',
        },
    },
    {
        path: '/notes',
        name: 'notes',
        component: () => import('../views/Notes/index.vue'),
        meta: {
            show: true,
            type: 'main',
        },
    },
];

export { mainNav };